#Brigit - The story creator#
Brigit is, *will be*, a program that takes in a formatted text file and creates
a scene in the game engine Unity. It is, *will be*, a quick way to create expansive
and deep exchanges between characters. All in all it is, *will be*, basically makes a choose your
own adventure game from a txt file that look like html with branching choices and
stuff.
